import boto3, io, base64, requests, configparser, os
from PIL import Image

CONFIG_FILENAME = 'settings.ini'
QUEUE_URL = ''
config = configparser.ConfigParser()
config.read(CONFIG_FILENAME)


# получение учетных данных от для amazon совместимых сервисов
def get_credentials():
    if not os.getenv('AWS_ACCESS_KEY_ID') or not os.getenv('AWS_SECRET_ACCESS_KEY'):
        if not ('AWS' in config):
            raise AttributeError('No credentials provided.')
        return {'AWS_ACCESS_KEY_ID': config['AWS']['AWS_ACCESS_KEY_ID'],
                'AWS_SECRET_ACCESS_KEY': config['AWS']['AWS_SECRET_ACCESS_KEY']}
    return {'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
            'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY')}


creds = get_credentials()
session = boto3.session.Session(aws_access_key_id=creds['AWS_ACCESS_KEY_ID'],
                                aws_secret_access_key=creds['AWS_SECRET_ACCESS_KEY'])
s3 = session.client(
    service_name='s3',
    endpoint_url='https://storage.yandexcloud.net'
)

sqs = session.client(
    service_name='sqs',
    endpoint_url='https://message-queue.api.cloud.yandex.net',
    region_name='ru-central1'
)


# загрухзка фотографии
def get_photo(bucket_name, object_id):
    photo = io.BytesIO()
    s3.download_fileobj(bucket_name, object_id, photo)
    photo.seek(0)
    return photo


# кодирование в файла в base64 для отправки
def encode_file(input_file):
    file_content = input_file.read()
    return base64.b64encode(file_content)


# запрос к api поиска лиц
def detect_faces(photo, key):
    headers = {"Authorization": f"Bearer {key}"}
    data = {
        "analyze_specs": [{"content": encode_file(photo).decode(), "features": [{"type": "FACE_DETECTION"}]}]}
    response = requests.post('https://vision.api.cloud.yandex.net/vision/v1/batchAnalyze', json=data, headers=headers)
    return response


# получение ссылки на очередь из файла конфигурации
def get_queue_url():
    if QUEUE_URL:
        return QUEUE_URL
    if not (config['YANDEX']['QUEUE_URL']):
        raise AttributeError('No credentials provided.')
    return config['YANDEX']['QUEUE_URL']


# вырезание лиц и загрузка в хранилище
def cut_and_upload(image_file, response, bucket_name, object_name):
    if 'faces' in response.json()['results'][0]['results'][0]['faceDetection'].keys():
        result = response.json()['results'][0]['results'][0]['faceDetection']['faces']
        image_file.seek(0)
        image = Image.open(image_file)
        id = 1
        message = {'original': object_name, 'faces': []}
        for face in result:
            bounds = face['boundingBox']['vertices']
            box = tuple(map(int, (bounds[0]['x'], bounds[0]['y'],
                                  bounds[2]['x'], bounds[2]['y'])))
            cropped = image.crop(box)
            upload_file = io.BytesIO()
            cropped.save(upload_file, format=image.format)
            # сброс оффсета
            upload_file.seek(0)
            upload_file_name = f'single/{object_name}/face-{id}-{object_name}'
            s3.upload_fileobj(upload_file, bucket_name, upload_file_name)
            message['faces'].append(upload_file_name)
            id += 1
        sqs.send_message(
            QueueUrl=get_queue_url(),
            MessageBody=str(message)
        )


# обработчик событий
def handler(event, context):
    object_id = event['messages'][0]['details']['object_id']
    bucket = event['messages'][0]['details']['bucket_id']
    if not object_id.startswith('single/'):
        photo_file = get_photo(bucket, object_id)
        response = detect_faces(photo_file, context.token['access_token'])
        print(response.text)
        print('Response:', response)
        if response.status_code != 200:
            raise PermissionError('Please check your API key.')
        cut_and_upload(photo_file, response, bucket, object_id)
    return {
        'statusCode': 200,
        'body': 'Ok',
    }
