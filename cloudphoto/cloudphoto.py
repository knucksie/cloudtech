import boto3, os, logging, configparser, click

CONFIG_FILENAME = 'settings.ini'

config = configparser.ConfigParser()
config.read(CONFIG_FILENAME)


def get_credentials():
    if not os.getenv('AWS_ACCESS_KEY_ID') or not os.getenv('AWS_SECRET_ACCESS_KEY'):
        if not ('AWS' in config):
            raise AttributeError('No credentials provided.')
        return {'AWS_ACCESS_KEY_ID': config['AWS']['AWS_ACCESS_KEY_ID'],
                'AWS_SECRET_ACCESS_KEY': config['AWS']['AWS_SECRET_ACCESS_KEY']}
    return {'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
            'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY')}


creds = get_credentials()
session = boto3.session.Session(aws_access_key_id=creds['AWS_ACCESS_KEY_ID'],
                                aws_secret_access_key=creds['AWS_SECRET_ACCESS_KEY'])
s3 = session.client(
    service_name='s3',
    endpoint_url='https://storage.yandexcloud.net'
)


def get_bucket_name():
    if not ('BUCKET' in config):
        raise AttributeError('No bucket name provided in config.')
    bucket_name = config['BUCKET']['bucket_name']
    buckets = [bucket['Name'] for bucket in s3.list_buckets()['Buckets']]
    if bucket_name not in buckets:
        s3.create_bucket(Bucket=bucket_name)
    return bucket_name

BUCKET_NAME = get_bucket_name()


def upload_photos(path, album):
    if not os.path.exists(path):
        raise ValueError('Specified directory does not exist.')
    for file in os.listdir(path):
        if os.path.isfile(os.path.join(path, file)) and (file.endswith('.jpg') or file.endswith('.jpeg')):
            s3.upload_file(os.path.join(path, file), BUCKET_NAME, f'{album}/{file}')


def get_photos(album):
    if album not in get_albums():
        raise ValueError('Specified album does not exist.')
    photos = []
    for key in s3.list_objects(Bucket=BUCKET_NAME)['Contents']:
        object_name = key['Key']
        if object_name.startswith(f'{album}/'):
            photos.append(object_name.lstrip(f'{album}/'))
    return photos


def download_photos(path, album):
    if not os.path.exists(path):
        raise ValueError('Specified directory does not exist.')
    if album not in get_albums():
        raise ValueError('Specified album does not exist.')
    for key in s3.list_objects(Bucket=BUCKET_NAME)['Contents']:
        object_name = key['Key']
        if object_name.startswith(f'{album}/'):
            s3.download_file(BUCKET_NAME, object_name, os.path.join(path, object_name.lstrip(f'{album}/')))


def get_albums():
    albums = set()
    for key in s3.list_objects(Bucket=BUCKET_NAME)['Contents']:
        object_name = key['Key']
        try:
            album, name = object_name.split('/')
            albums.add(album)
        except ValueError:
            if object_name.endswith('/'):
                logging.warning(f'Album {object_name} does not contain any photos.')
            else:
                logging.warning(f'File {object_name} does not belong to any album.')
    return albums


def list_albums():
    print(*get_albums(), sep='\n')


def list_photos(album):
    try:
        print(*get_photos(album), sep='\n')
    except ValueError as error:
        logging.error(error)


@click.command()
@click.argument('command')
@click.option('--album', '-a', default='/none', help='Desired album name.')
@click.option('--path', '-p', help='Path to directory. Path must exist.')
def cli(command, album, path):
    if command == 'list':
        if album == '/none':
            list_albums()
        else:
            list_photos(album)
    elif command == 'upload':
        if album == '/none':
            logging.error('No command name specified')
        elif path == '/none':
            logging.error('No path specified.')
        else:
            try:
                upload_photos(path, album)
            except ValueError as error:
                logging.error(error)
    elif command == 'download':
        if album == '/none':
            logging.error('No command name specified')
        elif path == '/none':
            logging.error('No path specified.')
        else:
            try:
                download_photos(path, album)
            except ValueError as error:
                logging.error(error)
    else:
        logging.error('Unknown command.')
