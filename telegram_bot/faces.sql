--
-- Файл сгенерирован с помощью SQLiteStudio v3.3.3 в Чт дек 30 02:35:42 2021
--
-- Использованная кодировка текста: System
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Таблица: chats
CREATE TABLE chats (id INTEGER PRIMARY KEY AUTOINCREMENT, telegram_chat_id BIGINT);

-- Таблица: people
CREATE TABLE people (id INTEGER PRIMARY KEY AUTOINCREMENT, name VARCHAR);

-- Таблица: people_photo
CREATE TABLE people_photo (id INTEGER PRIMARY KEY AUTOINCREMENT, person_id INTEGER REFERENCES people (id), photo_id INTEGER REFERENCES photos (id));

-- Таблица: photos
CREATE TABLE photos (id INTEGER PRIMARY KEY AUTOINCREMENT, object_id VARCHAR);

-- Таблица: photos_messages
CREATE TABLE photos_messages (id INTEGER PRIMARY KEY AUTOINCREMENT, message_id INTEGER, photo_id INTEGER REFERENCES photos (id));

COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
