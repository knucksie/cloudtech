import boto3, io, base64, requests, configparser, os, sqlite3, json, telebot

CONFIG_FILENAME = 'settings.ini'
API_KEY = ''
config = configparser.ConfigParser()
config.read(CONFIG_FILENAME)
DB_NAME = 'faces.db'


# получение учетных данных от s3 совместимых сервисов
def get_credentials():
    if not os.getenv('AWS_ACCESS_KEY_ID') or not os.getenv('AWS_SECRET_ACCESS_KEY'):
        if not ('AWS' in config):
            raise AttributeError('No credentials provided.')
        return {'AWS_ACCESS_KEY_ID': config['AWS']['AWS_ACCESS_KEY_ID'],
                'AWS_SECRET_ACCESS_KEY': config['AWS']['AWS_SECRET_ACCESS_KEY']}
    return {'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
            'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY')}


creds = get_credentials()
session = boto3.session.Session(aws_access_key_id=creds['AWS_ACCESS_KEY_ID'],
                                aws_secret_access_key=creds['AWS_SECRET_ACCESS_KEY'])
s3 = session.client(
    service_name='s3',
    endpoint_url='https://storage.yandexcloud.net'
)

sqs = session.client(
    service_name='sqs',
    endpoint_url='https://message-queue.api.cloud.yandex.net',
    region_name='ru-central1'
)


# функция загрузки фотографии из s3
def get_photo(bucket_name, object_id):
    photo = io.BytesIO()
    s3.download_fileobj(bucket_name, object_id, photo)
    photo.seek(0)
    return photo


def save_bucket_name(bucket_name):
    global config
    config['BUCKET']['BUCKET_NAME'] = bucket_name
    with open(CONFIG_FILENAME, 'w') as configfile:
        config.write(configfile)


def get_bucket_name():
    global config
    return config['BUCKET']['BUCKET_NAME']


# поулчение токена Telegram Bot API
def get_api_key():
    if API_KEY:
        return API_KEY
    if not (config['TELEGRAM']['BOT_TOKEN']):
        raise AttributeError('No credentials provided.')
    return config['TELEGRAM']['BOT_TOKEN']


# функция для запросов к БД sqlite
def request_info(sql):
    conn = sqlite3.connect(DB_NAME)
    cur = conn.cursor()
    cur.execute(sql)
    result = cur.fetchall()
    return result


# функция для записи в БД sqlite
def insert_info(sql):
    conn = sqlite3.connect(DB_NAME)
    cur = conn.cursor()
    cur.execute(sql)
    conn.commit()
    return True


def handler(event, context):
    # если пришёл запрос от Telegram Bot API через вебхук
    if 'httpMethod' in event.keys():
        body = json.loads(event["body"])
        reply_chat_id = body["message"]["chat"]["id"]
        if body["message"]["text"] == "/start":
            reply_text = "Вы успешно подписались!"
            insert_info(f'INSERT into chats(telegram_chat_id) values(\"{reply_chat_id}\")')
        elif body["message"]["text"].startswith("//find"):
            cmd, name = body["message"]["text"].split()
            person_id = request_info(f'SELECT id FROM people WHERE name = \"{name}\"')[0][0]
            if not person_id:
                reply_text = "Ой, а мы не знаем такого("
            else:
                photos = request_info(f'SELECT object_id FROM photos WHERE photos.id in (SELECT photo_id FROM '
                                      f'people_photo WHERE people_photo.person_id = {person_id}')
                if photos:
                    # пересылаем все фото, если нашли
                    tb = telebot.TeleBot(get_api_key())
                    for object_id in photos:
                        photo = get_photo(body['bucket_name'], object_id)
                        tb.send_photo(reply_chat_id, photo)
                    reply_text = f"Ищем {name} на фото. Вот, всё, что нашли."
                else:
                    # уведомляем, если не нашли фото, но есть информация о человеке в БД
                    reply_text = f"Странно, похоже {name} нет ни на одном фото. Хмм..."
        elif "reply_to_message" in body["message"].keys():
            message_id = body["message"]["reply_to_message"]["message_id"]
            name = body["message"]["text"]
            photo_id = request_info(f'SELECT photo_id FROM photos_messages WHERE message_id = {message_id}')[0][0]
            insert_info(f'INSERT into people(name) values(\"{name}\")')
            person_id = request_info(f'SELECT id FROM people WHERE name = \"{name}\"')[0][0]
            insert_info(f'INSERT into people_photo(person_id, photo_id) values({person_id}, {photo_id})')
            reply_text = "Спасибо! Информация принята к сведению."
        else:
            reply_text = "Команда не распознана."
        return {
            # всегда отвечаем на запросы пользователя
            'statusCode': 200,
            'headers': {
                'Content-Type': 'application/json'
            },
            'body': json.dumps(
                {
                    'method': 'sendMessage',
                    'chat_id': reply_chat_id,
                    'text': reply_text,
                }
            )
        }
    # если в s3 добавили новое фото
    else:
        body = json.loads(event['messages'][0]['details']['message']['body'])
        chats = request_info("""SELECT telegram_chat_id FROM chats""")
        tb = telebot.TeleBot(get_api_key())
        original_object_id = body['original']
        insert_info(f'INSERT into photos(object_id) values(\"{original_object_id}\")')
        # запоминаем имя бакета в конфиге
        save_bucket_name(body['bucket_name'])
        for chat_id in chats:
            for face in body['faces']:
                # загрузка из s3
                photo = get_photo(body['bucket_name'], face)
                # отправка фото с подписью
                sent_message = tb.send_photo(chat_id[0], photo, caption='Кто это?')
                original_photo_id = request_info(f'SELECT id FROM photos WHERE object_id = {original_object_id}')[0][0]
                # добавление информации об отправленных сообщениях в БД
                insert_info(
                    f'INSERT into photos_messages(photo_id, message_id) values({original_photo_id}, {sent_message.id} )')
        return {
            'statusCode': 200,
            'headers': {
                'Content-Type': 'text/plain'
            },
            'body': json.dumps(
                {
                    'text': 'Successfully requested names.',
                }
            )
        }
